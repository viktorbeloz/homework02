﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjController : MonoBehaviour
{
    [SerializeField]
    private int _hp;

    [SerializeField]
    private int DestroyPoints;

    private void Awake()
    {
        EventManager.AddListener(EventManager.COLLISION_TO_OBJ, ColToObjEventManager);
    }

    public void ColToObjEventManager(EventData eventData)
    {
        if(gameObject==(GameObject)eventData.data)
        {
            _hp -= 10;
            if(_hp<=0)
            {
                GameController.Score += DestroyPoints;
                Destroy(gameObject);
            }
        }
    }
    private void OnDestroy()
    {
        EventManager.RemoveEvent(EventManager.COLLISION_TO_OBJ,ColToObjEventManager);
    }

}
