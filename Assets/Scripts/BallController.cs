﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{

    private void OnCollisionEnter2D(Collision2D collision)
    {
        float xForce = 1;
        if (collision.gameObject.name == "Player")
        {
            EventManager.DispatchEvent(EventManager.COLLISION_TO_PLAYER, new EventData(true));
            Vector2 force = new Vector2(
                    (transform.position.x - collision.gameObject.transform.position.x) * 5, xForce);
            GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);
        }
        if (collision.gameObject.tag == "Lose")
        {
            EventManager.DispatchEvent(EventManager.COLLISION, new EventData(true));
        }
        if (collision.gameObject.tag == "obj")
        {
            EventManager.DispatchEvent(EventManager.COLLISION_TO_OBJ, new EventData(collision.gameObject));
        }
    }


}
