﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    public class EventData
    {
        public object data { get;}

        public EventData(object _data)
        {
            data = _data;
        }
    }
}
