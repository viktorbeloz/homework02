﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Text _counterText;
    private int _speed;

    void Start()
    {
        _speed = 5;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) && transform.position.x > -2.8f)
        {
            gameObject.transform.position -= new Vector3(_speed * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow) && transform.position.x < 2.8f)
        {
            gameObject.transform.position += new Vector3(_speed * Time.deltaTime, 0, 0);
        }

    }

}
