﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonsController : MonoBehaviour
{
    [SerializeField]
    private Button _startButton;

    void Start()
    {
        _startButton.onClick.AddListener(StartBtnClick);
    }
    private void StartBtnClick()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
