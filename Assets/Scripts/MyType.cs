﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts
{
    class MyType
    {
        public int score { get; set; }
        public bool isLose { get; set; }

        public MyType(int _score, bool _isLose)
        {
            score = _score;
            isLose = _isLose;
        }
    }
}
