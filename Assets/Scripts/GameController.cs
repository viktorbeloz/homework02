﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts;
using System;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Text _counterText;
    [SerializeField]
    private Button _restartButton;
    [SerializeField]
    private Button _exitButton;

    [SerializeField]
    private GameObject _player;
    [SerializeField]
    private GameObject _ball;
    [SerializeField]
    private GameObject _gameElements;
    [SerializeField]
    private GameObject _loseIcon;

    public static int Score = 0;
    private  int i;
    private void Awake()
    {
        EventManager.AddListener(EventManager.COLLISION,CollisionEventManager);
    }
    void Start()
    {
        _restartButton.onClick.AddListener(ClickRestart);
        _exitButton.onClick.AddListener(ClickExit);
    }

    private void ClickRestart()
    {
        Score = 0;
        SceneManager.LoadScene("SampleScene");
    }
    private void ClickExit()
    {
        SceneManager.LoadScene("Menu");
    }

    void FixedUpdate()
    {
        _counterText.text = $"Score: {Score}";
    }
    private void CollisionEventManager(EventData eventData)
    {
        if(Convert.ToBoolean(eventData.data))
        {
            Destroy(_player);
            Destroy(_gameElements);
            Destroy(_ball);
            _loseIcon.SetActive(true);            
        }
       
    }
    private void OnDestroy()
    {
        EventManager.RemoveEvent(EventManager.COLLISION,CollisionEventManager);
    }
}
